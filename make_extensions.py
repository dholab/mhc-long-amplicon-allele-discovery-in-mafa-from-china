#!/usr/bin/python

import re

def revcom(sequence):
	revDictDNA = {"a":"t", "t":"a", "g":"c", "c":"g", "A":"T", "T":"A", "G":"C", "C":"G"}
	revDictRNA = {"g":"c", "c":"g", "a":"u", "u":"a", "G":"C", "C":"G", "A":"U", "U":"A"}
	
		
	sequenceRev = sequence[::-1]
	sequenceRevCom = ""
	match = re.search('u|U',sequence)
        
	for char in sequenceRev:
		if match:
			newchar = revDictRNA[char]
			sequenceRevCom += newchar
		else:
			newchar = revDictDNA[char]
			sequenceRevCom += newchar
	return sequenceRevCom

with open('filtered_contigs_mapped.sam','r') as samFile:
	for line in samFile:
		if line[:1] != "@":
			samParts = line.split()
			orientation = samParts[1]
			alleleName = samParts[2]
			sequence = samParts[9]
			
			if orientation == "16":
				revcomplement = revcom(sequence)
				print('>',alleleName,'_EXTENSION_PT_MHCI_pacbio13_kent4',sep='')
				print(revcomplement)
			else:
				print('>',alleleName,'_EXTENSION_PT_MHCI_pacbio13_kent4',sep='')
				print(sequence)
				
				
				
				
				