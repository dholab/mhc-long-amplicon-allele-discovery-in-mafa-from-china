#!/usr/bin/python

from ReadFasta import ReadFasta

reader = ReadFasta('contigs.fasta')
collection = reader.openFasta()

for clusterHeader in collection:
	sizeAnnotation = clusterHeader.split(';')[1]
	sizeInteger = sizeAnnotation.split('=')[1]
	
	if int(sizeInteger) >= 5:
		print(clusterHeader)
		print(collection[clusterHeader])