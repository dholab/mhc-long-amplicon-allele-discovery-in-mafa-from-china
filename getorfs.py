#!/usr/bin/python

import re
from ReadFasta import ReadFasta
import glob
from subprocess import call

fastqfileNames = glob.glob('*.fasta')
for filename in fastqfileNames:
	filenameHead = filename.split('.')[0]
	call(['getorf','-minsize','900','-maxsize','1200','-find','3','-sequence',filename,'-outseq',
	filename + '.orfs.fasta'])
	call(['getorf','-minsize','900','-maxsize','1200','-find','6','-flanking','0','-sequence',filename,
	'-outseq',filenameHead + '.stop.fasta'])


orfFileNames = glob.glob('*.orfs.fasta')

for orfFile in orfFileNames:
	fileHead = orfFile.split('.')[0]
	finalOrfFile = open(fileHead + '.orfs.stop.fasta','w')
	
	#open the stop codon file, make dictionary by name, stop codon		
	#stopReader = ReadFasta('lbc2.stop.fasta')
	stopReader = ReadFasta(fileHead + '.stop.fasta')
	stopCollection = stopReader.openFasta()
	
	
	#orfsReader = ReadFasta('lbc2.merged.zerolen.fastq.appended.orfs.fasta')
	orfsReader = ReadFasta(orfFile)
	orfsCollection = orfsReader.openFasta()
	
	editedStopDict = {}
	
	for stopname in stopCollection:
		#name = re.search("^(>\w+.merged.zerolen.fastq_1)",stopname)
		name = re.search("^(>.+.zerolen.fastq)",stopname)
		
		#print(stopname)
		if name:
			actualName = name.group(1)
			editedStopDict[actualName] = stopCollection[stopname]
	
	for orf in orfsCollection:
		#name = re.search("^(>\w+.merged.zerolen.fastq_1)",orf)
		name = re.search("^(>.+.zerolen.fastq)",orf)
		if name:
			newName = name.group(1)
			if newName in editedStopDict:
			#editedOrfsDict[newName] = orfsCollection[orf]
				finalOrfFile.write(newName + '\n')
				finalOrfFile.write(orfsCollection[orf] + editedStopDict[newName] + '\n')
			else:
				print(newName)

