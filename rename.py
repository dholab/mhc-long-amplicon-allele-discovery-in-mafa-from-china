#!/usr/bin/python

from ReadFasta import ReadFasta
import re

reader = ReadFasta('271_contigs_deduped.fasta')
collection = reader.openFasta()

contigCount = 1

for name in collection:
	#print(name)
	nameMatch = re.search('(size=\d+)',name)
	if nameMatch:
		sizeString = nameMatch.group(1)
		print('>Contig',contigCount,'_',sizeString,sep='')
	else:
		print('>Contig',contigCount,sep='')
	print(collection[name])
	contigCount += 1