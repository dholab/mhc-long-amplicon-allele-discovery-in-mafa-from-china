#!/usr/bin/python

import re

from ReadFasta import ReadFasta

reader = ReadFasta('contigs.notextensions.fasta')
collection = reader.openFasta()


contigCount = 1
for header in collection:
	matchObj = re.search('ccs_(\w+--\w+).zerolen',header)
	barcodeNumber = matchObj.group(1)
	
	matchObjSize = re.search('(size=\d+)',header)
	size = matchObjSize.group(1)
	
	print('>Contig_',contigCount,'_',barcodeNumber,':',size,sep='')
	print(collection[header])
	contigCount += 1