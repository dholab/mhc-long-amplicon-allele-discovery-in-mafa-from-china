#!/usr/bin/python
import re

alleleCountsDict = {}

lineNum = 0

with open('rawreads_map_pac7_48samples_perfect.sam','r') as samFile:
	for line in samFile:
		lineNum += 1
		if line[:1] != "@" and line[:1] == "m":
			samParts = line.split()
			barcodeFilename = samParts[0]
			bcNumberMatch = re.search('(\w+--\w+).zerolen.fastq(\w+)',barcodeFilename)
			#print(bcNumberMatch.group(1),samParts[2])
			alleleName = samParts[2]
			#alleleNameParts = alleleName.split('-')
			alleleString = alleleName
			#if len(alleleNameParts) == 4:
			#	alleleString = alleleNameParts[2] + '-' + alleleNameParts[3] + '-' + alleleNameParts[0] + '-' + alleleNameParts[1]
			#else:
			#	alleleString = alleleNameParts[2] + '-' + alleleNameParts[0] + '-' + alleleNameParts[1]

			barcode = bcNumberMatch.group(1) + bcNumberMatch.group(2)
			#print(lineNum)
			
			if not barcode in alleleCountsDict:
				alleleCountsDict[barcode] = {alleleString : 1}
			elif alleleString not in alleleCountsDict[barcode]:
				alleleCountsDict[barcode][alleleString] = 1
			else:
				alleleCountsDict[barcode][alleleString] += 1

#count = 0

#for bc in alleleCountsDict:
#	count += 1
#	for alleleName in alleleCountsDict[bc]:
	#	print(bc,alleleName,alleleCountsDict[bc][alleleName],sep='\t')
allAllelesDict = {}

for bc in alleleCountsDict:
	for alleleName in alleleCountsDict[bc]:
		if not alleleName in allAllelesDict:
			allAllelesDict[alleleName] = 1
		
	
print("allele_name",end='\t')
for bc in sorted(alleleCountsDict):
	print(bc,end='\t')
print()

for aName in sorted(allAllelesDict):
	print(aName,end='\t')
	for bc in sorted(alleleCountsDict):
		if aName in alleleCountsDict[bc]:
			print(alleleCountsDict[bc][aName],end='\t')
		else:
			print('\t',end='')
	print()

	
		
		
		
#print(count)